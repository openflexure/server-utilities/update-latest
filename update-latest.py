#!/usr/bin/python3

import os
import glob
import re
import argparse
from pprint import pprint

parser = argparse.ArgumentParser(description='Update the internal Nginx redirect map')
parser.add_argument('--dry', dest='dry', action='store_true', help='Perform a dry-run')

args = parser.parse_args()

# Natural sorting functions
def natsort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    return sorted(l, key=alphanum_key)

# Global functions
def map_to_dict(map_path):
    d = {}
    if not os.path.isfile(map_path):
        return {}
    with open(map_path) as f:
        for line in f:
            (key, val) = line.strip(';\n').split(' ')
            d[key] = val
    
    return d

def dict_to_map(map_dictionary):
    s = ""

    for key, val in map_dictionary.items():
        s += "{} {};\n".format(key, val)
    
    return s

def save_map(map_string, map_path):
    print("Writing:")
    print(map_string)
    print("To file:")
    print(map_path)
    
    with open(map_path, 'w') as f:
        f.write(map_string)
    

def find_latest_date(file_dir, file_template):
    f_paths = glob.glob("{}/{}".format(file_dir, file_template))
    f_files = [f_path.split('/')[-1] for f_path in f_paths]
    f_files.sort()

    if f_files:
        return f_files[-1]
    else:
        return None

def version_extractor(template):
    """Return a function that extracts the version from a filename, matching the given template.
    
    Version numbers must include three numbers separated by '.' and an optional suffix.  If
    a . or - character is present before the suffix, it is ignored.

    """
    def extract_version(fname):
        """Extract version numbers from a filename"""
        m = re.match(template.format(version=r"(0|[1-9]\d*).(0|[1-9]\d*).(0|[1-9]\d*)(([^\d].*)?)"), fname)
        if not m:
            print(f"{fname} did not match the version format defined in `version_extractor`")
            return None
        else:
            # Convert the first 3 components to integers for proper ordering
            version = (int(m.group(1)), int(m.group(2)), int(m.group(3)), m.group(4))
            return version
    return extract_version
    

def find_latest_version(file_dir, file_template, prerelease=False):
    f_paths = glob.glob("{}/{}".format(file_dir, file_template.format(version="*")))
    f_files = [f_path.split('/')[-1] for f_path in f_paths]
    # If the extracted version is None, it didn't match.
    # The third component of the version is the suffix, which should be empty
    # for non-prerelease versions.
    extract_version = version_extractor(file_template)
    # Filter the files, so we only take those matching a valid version
    f_files = [f for f in f_files if extract_version(f)]
    # Now, filter for pre-release, or non-pre-release, versions
    if prerelease:
        f_files = [f for f in f_files if extract_version(f)[3]]
    else:
        f_files = [f for f in f_files if not extract_version(f)[3]]

    f_files.sort(key=extract_version)

    if f_files:
        return f_files[-1]
    else:
        return None

def find_latest_prerelease(file_dir, file_template):
    return find_latest_version(file_dir, file_template, prerelease=True)

# Set up some global stuff
map_path = "/var/www/build/redirects-map.conf"
old_map = map_to_dict(map_path)
new_map = map_to_dict(map_path)



#### We will first start with the raspbian-openflexure builds

# Where the versioned files are located
file_dir = "/var/www/build/raspbian-openflexure"
# Glob template for versioned files
file_template = "*-raspbian-openflexure-buster-armhf-full.zip"

base_path = "/raspbian-openflexure"
templates = {
    "*-raspbian-openflexure-buster-armhf-full.zip": ["{}/latest".format(base_path), "{}/armhf/latest".format(base_path)],
    "*-raspbian-openflexure-buster-armhf-lite.zip": ["{}/armhf/lite/latest".format(base_path)]
}

for file_template, deploy_paths in templates.items():
    latest_file = find_latest_date(file_dir, file_template)
    for deploy_path in deploy_paths:
        new_map[deploy_path] = "{}/{}".format(base_path, latest_file)


#### Now we move on to Connect/eV builds

file_dir = "/var/www/build/openflexure-ev"

# URI of the project, relative to host root
deploy_path = "/openflexure-ev"

targets = {
    'win': "openflexure-connect-{version}-win.exe",
    'win/portable': "openflexure-connect-{version}-win-portable.exe",
    'linux/x86_64': "openflexure-connect-{version}-linux-x86_64.AppImage",
    'linux/armv7l': "openflexure-connect-{version}-linux-armv7l.AppImage"
}

for target, file_template in targets.items():
    # Find latest
    latest_file = find_latest_version(file_dir, file_template)

    # Add to map
    new_map["{}/latest/{}".format(deploy_path, target)] = "{}/{}".format(deploy_path, latest_file)



#### Move on to microscope STL builds

file_dir = "/var/www/build/openflexure-microscope"

# URI of the project, relative to host root
deploy_path = "/openflexure-microscope"

# Find latests
latest_folder = find_latest_version(file_dir, "v{version}")
latest_folder = latest_folder if latest_folder else "master"  # If no version exists, redirect to master

latest_zip = find_latest_version(file_dir, "openflexure-microscope-v{version}.zip")
latest_zip = latest_zip if latest_zip else "openflexure-microscope-master.zip"  # If no version exists, redirect to master

# Add to map
new_map["~{}/latest/(.*)".format(deploy_path)] = "{}/{}/$1".format(deploy_path, latest_folder)
new_map["{}/latest".format(deploy_path)] = "{}/{}".format(deploy_path, latest_folder)
new_map["{}/latest/all".format(deploy_path)] = "{}/{}".format(deploy_path, latest_zip)


#### Delta-stage builds

file_dir = "/var/www/build/openflexure-delta-stage"

# URI of the project, relative to host root
deploy_path = "/openflexure-delta-stage"

# Find latests
latest_folder = find_latest_version(file_dir, "v{version}")
latest_folder = latest_folder if latest_folder else "master"  # If no version exists, redirect to master

# Add to map
new_map["~{}/latest/(.*)".format(deploy_path)] = "{}/{}/$1".format(deploy_path, latest_folder)
new_map["{}/latest".format(deploy_path)] = "{}/{}".format(deploy_path, latest_folder)


#### Now we move on to server builds

file_dir = "/var/www/build/openflexure-microscope-server"

# URI of the project, relative to host root
deploy_path = "/openflexure-microscope-server"

# Find latest
latest_file = find_latest_version(file_dir, "openflexure-microscope-server-v{version}.tar.gz")

# Add to map
new_map["{}/latest".format(deploy_path)] = "{}/{}".format(deploy_path, latest_file)

# Find latest prerelease
latest_pre = find_latest_prerelease(file_dir, "openflexure-microscope-server-v{version}.tar.gz")

# Add to map
new_map["{}/latest/pre".format(deploy_path)] = "{}/{}".format(deploy_path, latest_pre)


##### Finally, save the redirect map to disk

if not args.dry:
    if old_map != new_map:
        save_map(dict_to_map(new_map), map_path)
    else:
        print("Map unchanged, skipping write")
else:
    pprint(new_map)
    print("Dry run, skipping write")
