# update-latest

Update our redirect map file based on the most recent builds found in /var/www/build

## Usage

On the build server, this script should run every time a new OpenFlexure eV or Raspbian-OpenFlexure build is deployed.

This can be done via SSH from GitLab CI, or as an additional step in an on-server cron job.